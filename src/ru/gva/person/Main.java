package ru.gva.person;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Демонстрационный класс для классов Student и Teacher,
 * содержит в себе методы позволяющие найти преподователя с группой,
 * так же содержит методы позволяющие найти девушек поступивших в колледж в 2014 году.
 *
 * @author Gavrikov V.A. group 15OIT18
 */
public class Main {
	static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
	    System.out.println("Введите кол-во преподователей - ");
	    ArrayList<Teacher> teachers = new ArrayList<>(sc.nextInt());
		System.out.println("Введите кол-во студентов - ");
		ArrayList<Student> students = new ArrayList<>(sc.nextInt());
		input(students);
		input2(teachers);
		System.out.println(sorting(teachers));
		System.out.println(sorting1(students));

    }

	/**
	 * Данный метод создает обьек типа Student, добавляет его в динамический список,
	 * и вызывает метод для заполнения данного обьекта.
	 *
	 * @param students ArrayList динамический список который будет хранить информацию о студентах.
	 */
    public static void input(ArrayList<Student> students){
		for (int i = 0; i <students.size(); i++) {
			students.add(new Student());
			students.get(i).input();
		}
	}

	/**
	 * Данный метод создает обьек типа Teacher, добавляет его в динамический список,
	 * и вызывает метод для заполнения данного обьекта.
	 *
	 * @param teachers ArrayList динамический список который будет хранить информацию о преподователей

	 */
    public static void input2(ArrayList<Teacher> teachers){
		for(int i = 0; i<teachers.size();i++){
			teachers.add(new Teacher());
			teachers.get(i).input();
		}

	}

	/**
	 * Данный метод считает кол-во девушек поступивших в колледж в 2014 году.
	 *
	 * @param students динамический список который содержит информацию о студентах.
	 */
	private static int sorting1(ArrayList<Student> students) {
		int quantity1= 0;
		for (int i = 0; i <students.size() ; i++) {
			if (students.get(i).getGender() == Gender.WOMAN &&students.get(i).getYearOfStart()== 2014){
				quantity1++;
			}

		}
		return quantity1;
	}


	/**
	 * Данный метод считает кол-во преподователей у которых есть группа.
	 *
	 * @param teachers динамический список который содержит информацию о преподователях.
	 */
	private static int sorting(ArrayList<Teacher> teachers) {
		int quantity = 0;
		for (int i = 0; i <teachers.size() ; i++) {
			if (teachers.get(i).isCurator()==true){
				quantity++;
			}

		}
		return quantity;
	}
}

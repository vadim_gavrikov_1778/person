package ru.gva.person;

import java.util.Scanner;

/**
 * Родительский класс, содержащий поля
 *
 * @author Gavrikov V.A. group 15OIT18
 */
public class Person {
    Scanner sc = new Scanner(System.in);
    private Gender gender;
    private String name;
    private int year;


    public Person(Gender gender, String name, int year) {
        this.gender = gender;
        this.name = name;
        this.year = year;
    }

    public Person() {
        this(Gender.MAN, "", 0);
    }

    public Gender getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void input() {
        System.out.println("Введите фамилию:");
        this.setName(sc.next());
        System.out.println("Введите пол:");
        System.out.println("1 - Man, 2 - Woman");
        int gender = sc.nextInt();
        gender1(gender);
        System.out.println("Введите год рождения:");
        this.setYear(sc.nextInt());
    }

    public void gender1(int gender) {
        switch (gender) {
            case 1:
                this.setGender(Gender.MAN);
                break;
            case 2:
                this.setGender(Gender.WOMAN);
                break;
        }

    }
}

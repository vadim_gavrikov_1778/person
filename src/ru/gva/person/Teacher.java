package ru.gva.person;

import java.util.Scanner;

/**
 * Данный дочерний класс содержит обьект Teacher
 *
 * @author Gavrikov V.A. group 15OIT18
 */
public class Teacher extends Person {
    static Scanner sc = new Scanner(System.in);
    private boolean curator;
    private String discipline;
    private Categories categories;

    public Teacher(Gender gender, String surname, int year, boolean curator, String discipline,Categories categories) {
        super(gender, surname, year);
        this.curator = curator;
        this.discipline = discipline;
		this.categories = categories;
    }

    public boolean isCurator() {
        return curator;
    }

    public Teacher() {
        this(Gender.MAN,"",0,false,"",Categories.UNCATEGORIEZ);
    }

    /**
     * Метод для ввода полей обьекта.
     */

    public void input() {
        super.input();
        System.out.println("Введите дисциплину: ");
        this.discipline = this.sc.next();
        System.out.println("Классный руководитель: ");
        System.out.println("1 это true, 2 это false");
		boolean1(sc.nextInt());
		System.out.println("Введите категорию: ");
        System.out.println("1-FIRST, 2-HIGHEST, 3-UNCATEGORIEZ");
        categories(sc.nextInt());
    }
	
    public void boolean1(int boolean1){
	     switch (boolean1) {
            case 1:
                this.curator = true;
                break;
            case 2:
                this.curator = false;
                break;
        }
	}
	
    public void categories(int categories){
	    switch (categories){
                case 1:
                    this.categories = Categories.FIRST;
                    break;
                case 2:
                    this.categories = Categories.HIGHEST;
                    break;
                case 3:
                    this.categories = Categories.UNCATEGORIEZ;
                    break;
        }
	}


    @Override
    public String toString() {
        return "Преподаватель:" +
                "Фамилия='" + getName() +
				", Пол='" + getGender() +
                ", Год рождения=" + getYear() +
                " Классный руководитель=" + curator +
                ", Дисциплина='" + discipline + '\'' +
                ", Категория=" + categories +
                '}';
    }
}


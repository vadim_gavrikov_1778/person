package ru.gva.person;

/**
 * Данный дочерний класс содержит обьект Student
 *
 * @author Gavrikov V.A. group 15OIT18
 */
public class Student extends Person {
    private int yearOfStart;
    private String codeOfProfession;

    public Student(Gender gender, String surname, int year, int yearOfStart, String codeOfProfession) {
        super(gender, surname, year);
        this.yearOfStart = yearOfStart;
        this.codeOfProfession = codeOfProfession;
    }

    public Student() {
        this(Gender.MAN, "", 0, 0, "");
    }

    /**
     * Метод для ввода полей обьекта.
     */
    public void input() {
        super.input();
        System.out.println("Введите год поступления:");
        this.yearOfStart = sc.nextInt();
        System.out.println("Введите код специальности:");
        this.codeOfProfession = sc.next();
    }

    public int getYearOfStart() {
        return yearOfStart;
    }

    @Override
    public String toString() {
        return "Студент:" +
                " Пол=" + getGender() +
                ", Фамилия=" + getName() +
                ", Год рождения=" + getYear() +

                ", Год поступления= " + yearOfStart +
                ", Код профессии= " + codeOfProfession + '\'' +
                '}';
    }
}
